require(['../node_modules/@atlassian/aui-adg/lib/js/aui/flag'], function(flag) {
    flag({
        type: 'info',
        title: 'Issue ADG-745 has been created.',
        persistent: false,
        body:   '<ul class="aui-nav-actions-list">' +
        '    <li><a href="#">View issue</a></li>' +
        '    <li><a href="#">Add to sprint</a></li>' +
        '</ul>'
    });
});
