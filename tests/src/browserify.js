import auiFlag from '@atlassian/aui/lib/js/aui/flag';
import auiAdgFlag from '@atlassian/aui-adg/lib/js/aui/flag';

it('AUI flags work as expected', function () {
    expect(auiFlag).to.be.defined;
    var flag = auiFlag({});
    expect(flag.nodeType).to.be.defined;
});

it('AUI-ADG flags work as expected', function () {
    expect(auiAdgFlag).to.be.defined;
    var flag = auiAdgFlag({});
    expect(flag.nodeType).to.be.defined;
});
